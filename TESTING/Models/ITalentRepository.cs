﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TESTING.Models
{
    public interface ITalentRepository
    {
        IEnumerable<Talents> GetAll();
        Talents Get(int id);
        Talents Add(Talents item);
        void Remove(int id);
        bool Update(Talents item);
    }
}
