﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Stripe;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using TESTING.Models;

namespace TESTING.Controllers
{
    public class HomeController : Controller
    {
        string accessKey = WebConfigurationManager.AppSettings["AWSAccessKey"].ToString();
        string secretKey = WebConfigurationManager.AppSettings["AWSSecretKey"].ToString();
        string sessionToken = WebConfigurationManager.AppSettings["AWSSessionToken"].ToString();


        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        [System.Web.Mvc.HttpPost]
        public string SetTalentDetails(string model)
        {
            string connString = string.Format("Server=userdb.cbdrq6pvkvph.us-east-1.rds.amazonaws.com; database=db1; UID=admin; password=password; SslMode = none");

            /* var Name = HttpContext.Current.Request.Params["Name"];
             var ShortName = HttpContext.Current.Request.Params["ShortName"];
             var Reknown = HttpContext.Current.Request.Params["Reknown"];
             var Bio = HttpContext.Current.Request.Params["Bio"];*/
            var resumeDto = JsonConvert.DeserializeObject<Talents>(model);

            MySqlCommand cmd;
            MySqlConnection connection = new MySqlConnection(connString);

            //string insertQuery = "INSERT into user(Name,Email,Password) values ('" + obj.Name + "','" + obj.Email + "','" + obj.Password +"'); ";
            string insertQuery = "INSERT into talents(Name,ShortName,Reknown,Bio) VALUES (@Name,@ShortName,@Reknown, @Bio);";
            try
            {
                AmazonS3Client client = new AmazonS3Client(accessKey, secretKey, sessionToken, RegionEndpoint.APSoutheast1);
                var stream = Base64ToImage(resumeDto.LocalBase64);
                var fileTransferUtilityRequest = new TransferUtilityUploadRequest
                {
                    BucketName = "talentimages",
                    Key = resumeDto.Name,
                    CannedACL = S3CannedACL.PublicRead,
                    StorageClass = S3StorageClass.Standard,
                    InputStream = stream
                };
                var fileTransferUtility = new TransferUtility(client);
                fileTransferUtility.Upload(fileTransferUtilityRequest);

                connection.Open();
                cmd = new MySqlCommand(insertQuery, connection);
                cmd.Parameters.AddWithValue("@Name", resumeDto.Name);
                cmd.Parameters.AddWithValue("@ShortName", resumeDto.ShortName);
                cmd.Parameters.AddWithValue("@Reknown", resumeDto.Reknown);
                cmd.Parameters.AddWithValue("@Bio", resumeDto.Bio);

                // cmd = new MySqlCommand(insertQuery, cnn);
                cmd.ExecuteNonQuery();

                connection.Close();

                return "Successful in uploading the file";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return "Error";
            }
        }

        public Stream Base64ToImage(string base64String)
        {
            byte[] byteArray = Encoding.ASCII.GetBytes(base64String);
            MemoryStream stream = new MemoryStream(byteArray);
            return stream;
        }

        public ActionResult Charge()
        {
            ViewBag.Message = "Processing payments with Stripe";
            return View(new StripeChargeModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Charge(StripeChargeModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var chargeId = await ProcessPayment(model);
            // You should do something with the chargeId --> Persist it maybe?

            return View("Index");
        }

        private static async Task<string> ProcessPayment(StripeChargeModel model)
        {
            return await Task.Run(() =>
            {
                var myCharge = new StripeChargeCreateOptions
                {
                    // convert the amount of £12.50 to pennies i.e. 1250
                    Amount = (int)(model.Amount * 100),
                    Currency = "SGD",
                    Description = "Test Charge",
                    Source = new StripeSourceOptions
                    {
                        TokenId = model.Token
                    }
                };

                var chargeService = new StripeChargeService("sk_test_y3YQJMCmmdsKazgkYB1C0eJo00LLS2nzBJ");  // Private Key From Stripe Dashboard Account
                var stripeCharge = chargeService.Create(myCharge);

                return stripeCharge.Id;
            });
        }
    }
}
